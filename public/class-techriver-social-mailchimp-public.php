<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://apysais.com
 * @since      1.0.0
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/public
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Techriver_Social_Mailchimp_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Techriver_Social_Mailchimp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Techriver_Social_Mailchimp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/techriver-social-mailchimp-public.css', array(), $this->version, 'all' );
		
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Techriver_Social_Mailchimp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Techriver_Social_Mailchimp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/techriver-social-mailchimp-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( 
			$this->plugin_name, 'trjs',
			array( 
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'FB_API' => FB_API,
				'FB_Success_Msg' => 'Thank you for raising your voice and adding your name to this important fight. We will contact your representatives in Congress and pass along your support for the Muslim Brotherhood Terrorist Designation Act.'
			)
		);
	}

}
