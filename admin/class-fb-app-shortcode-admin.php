<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://apysais.com
 * @since      1.0.0
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/admin
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Techriver_FB_App_Shortcode_Admin {
	protected $mailchimp_instance = null;
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	public function get_list(){
		$list_array = array();
		$result = $this->mailchimp_instance->get('lists');
		if( $result && isset($result['total_items']) > 0 ){
			foreach($result['lists'] as $k => $v){
				$id = $v['id'];
				$name = $v['name'];
				$list_array[$id] = $name;
			}
		}
		return $list_array;
	}
	
	/**
	 * Add shortcode JS to the page
	 *
	 * @return HTML
	 */
	public function js_shortcodes(){
		?>
			<script type="text/javascript">
				function tr_add_fb_login(editor){
					var mc_list = [
						<?php if( count($this->get_list()) > 0 ){ ?>
								<?php foreach($this->get_list() as $key=>$val){ ?>
										{text: '<?php echo $val; ?>',value: '<?php echo $key;?>'},
								<?php } ?>
						<?php } ?>
					];
					var submenu_array =
					{
						text: 'Add FB Login for Mailchimp API',
						onclick: function() {
							editor.windowManager.open( {
								title: 'Choose Mailchimp List',
								width:980,
								height:350,
								body: [
									{
										type: 'listbox',
										name: 'listboxMCList',
										label: 'Choose Mailchimp List',
										'values': mc_list
									},
								],
								onsubmit: function( e ) {
									var template_path = ' mclist="' + e.data.listboxMCList + '" ';
									editor.insertContent(
										'[tr_fb_login_mailchimp ' + template_path + ']'
									);
								}
							});
						}
					};
					return submenu_array;
				}
			</script>
		<?php
	}
	
	public function init_shortcode($atts){
		$mclist = '';
		if( isset($atts['mclist']) ){
			$mclist = $atts['mclist'];
		}
		$a = shortcode_atts( array(
			'mclist' => $mclist,
		), $atts );
		$fb_app_shortcode_mclist = $a['mclist'];
		ob_start();
		require_once(TR_PLUGIN_PATH . 'admin/partials/fb-app-mailchimp.php');
		$output = ob_get_clean();
		return $output;
	}
	
	public function __construct(){
		$this->mailchimp_instance = new \DrewM\MailChimp\MailChimp(MAILCHIMP_API);
		add_action('admin_footer', array($this, 'js_shortcodes'));
		add_shortcode('tr_fb_login_mailchimp', array($this,'init_shortcode'));
	}

}
