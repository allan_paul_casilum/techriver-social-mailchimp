<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://apysais.com
 * @since      1.0.0
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/admin
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Techriver_Social_Mailchimp_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Techriver_Social_Mailchimp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Techriver_Social_Mailchimp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/techriver-social-mailchimp-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Techriver_Social_Mailchimp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Techriver_Social_Mailchimp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, 
			plugin_dir_url( __FILE__ ) . 'js/techriver-social-mailchimp-admin.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);
	}
	
	/**
     * Create a shortcode button for tinymce
     *
     * @return [type] [description]
     */
    public function shortcode_button(){
        if( current_user_can('edit_posts') &&  current_user_can('edit_pages') ){
            add_filter( 'mce_external_plugins', array($this, 'add_buttons' ));
            add_filter( 'mce_buttons', array($this, 'register_buttons' ));
        }
    }
    
     
    /**
     * Add new Javascript to the plugin scrippt array
     *
     * @param  Array $plugin_array - Array of scripts
     *
     * @return Array
     */
    public function add_buttons( $plugin_array ){
		$plugin_array['trshortcodes'] = plugin_dir_url( __FILE__ ) . 'js/tr-shortcode.js';
		return $plugin_array;
    }
    
    /**
     * Add new button to tinymce
     *
     * @param  Array $buttons - Array of buttons
     *
     * @return Array
     */
    public function register_buttons( $buttons ){
        array_push( $buttons, 'separator', 'trshortcodes' );
        return $buttons;
    }

}
