<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://apysais.com
 * @since      1.0.0
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
