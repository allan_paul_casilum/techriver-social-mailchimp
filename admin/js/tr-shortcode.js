(function() {
	tinymce.create('tinymce.plugins.TRshortcode', {
          init : function(editor, url) {
					var menu_button_label = 'Techriver';
					var list_menu = [
						{
							text:'APP',
							menu:[
								tr_add_fb_login(editor)
							]
						}
					];

					editor.addButton('trshortcodes', {
						type: 'menubutton',
						icon: false,
						text: menu_button_label,
						menu: list_menu
					});
          },
          createControl : function(n, cm) {
               return null;
          }
     });
	/* Start the buttons */
    tinymce.PluginManager.add('trshortcodes', tinymce.plugins.TRshortcode );
})();

