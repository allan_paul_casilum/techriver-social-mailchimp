<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://apysais.com
 * @since             1.0.0
 * @package           Techriver_Social_Mailchimp
 *
 * @wordpress-plugin
 * Plugin Name:       Techriver Social Mailchimp
 * Plugin URI:        http://techriver.net
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Allan Paul Casilum
 * Author URI:        http://apysais.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       techriver-social-mailchimp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
define('MAILCHIMP_API', 'e8d3871ab9c4d8561923cc445a5b8213-us12');//CJ
//define('MAILCHIMP_API', '8988e19c72654549c542feb75ea35ea6-us1');//TR
define('FB_API', '974976152582124');//CJ
//define('FB_API', '974163039335808');//Local
define('TR_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
function tr_plugin_data(){
	require_once(ABSPATH.'wp-admin/includes/plugin.php');
	$plugin_data = get_plugin_data( __FILE__ );
	return $plugin_data;
}
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-techriver-social-mailchimp-activator.php
 */
function activate_techriver_social_mailchimp() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-techriver-social-mailchimp-activator.php';
	Techriver_Social_Mailchimp_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-techriver-social-mailchimp-deactivator.php
 */
function deactivate_techriver_social_mailchimp() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-techriver-social-mailchimp-deactivator.php';
	Techriver_Social_Mailchimp_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_techriver_social_mailchimp' );
register_deactivation_hook( __FILE__, 'deactivate_techriver_social_mailchimp' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-techriver-social-mailchimp.php';
require plugin_dir_path( __FILE__ ) . 'includes/MailChimp.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_techriver_social_mailchimp() {

	$plugin = new Techriver_Social_Mailchimp();
	Techriver_FB_App_Shortcode_Admin::get_instance();
	$plugin->run();
		
}
add_action( 'wp_ajax_mailchimp_fb', 'mailchimp_fb_callback' );
add_action( 'wp_ajax_nopriv_mailchimp_fb', 'mailchimp_fb_callback' );
function mailchimp_fb_callback() {
	$mailchimp_array_error = array(
		400,
		401,
		403,
		404,
		405,
		414,
		422,
		429,
		500,
		503
	);
	$mclist = '0';
	if( isset($_POST['mclist']) ){
		$mclist = $_POST['mclist'];
	}
	$name = '';
	if( isset($_POST['name']) ){
		$name = $_POST['name'];
	}
	$email = '';
	if( isset($_POST['email']) ){
		$email = $_POST['email'];
	}

	//use \DrewM\MailChimp\MailChimp;
	$MailChimp = new \DrewM\MailChimp\MailChimp(MAILCHIMP_API);

	$list_id = $mclist;//counterjihad
	$subscribe_result = $MailChimp->post("lists/$list_id/members", [
		'email_address' => $email,
		'status'        => 'subscribed',
		'merge_fields' => array(
			'FNAME' => $name,
			'LNAME' => 'from facebook'
		)
	]);
	if( in_array($subscribe_result['status'], $mailchimp_array_error, true) ){
		echo 0;
	}else{
		//create cookies
		//cj_mailchimp_optin
		//expire 7 days
		setcookie( 'cj_mailchimp_optin', 1, time() + ( 7 * DAY_IN_SECONDS ) );
		echo 1;
	}
	
	wp_die(); // this is required to terminate immediately and return a proper response
}
run_techriver_social_mailchimp();
