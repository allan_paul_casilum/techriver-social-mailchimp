<?php

/**
 * Fired during plugin activation
 *
 * @link       http://apysais.com
 * @since      1.0.0
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/includes
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Techriver_Social_Mailchimp_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
