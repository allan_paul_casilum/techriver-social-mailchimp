<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://apysais.com
 * @since      1.0.0
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/includes
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Techriver_Social_Mailchimp_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
