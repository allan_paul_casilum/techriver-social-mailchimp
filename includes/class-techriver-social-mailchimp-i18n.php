<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://apysais.com
 * @since      1.0.0
 *
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Techriver_Social_Mailchimp
 * @subpackage Techriver_Social_Mailchimp/includes
 * @author     Allan Paul Casilum <allan.paul.casilum@gmail.com>
 */
class Techriver_Social_Mailchimp_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'techriver-social-mailchimp',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
